# Netflow install and setup
## Use
```bash
cd example

# Edit inventory
vim inventory/hosts

# Edit configs
vim inventory/group_vars/collector.yml
vim inventory/group_vars/sender.yml

# Install collector
ansible-playbook install_collector.yml

# Install sender
ansible-playbook install_sender.yml
```
